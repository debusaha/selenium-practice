package Selenium_Java;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChatWappTest {
	
	public static void main(String[] args) throws InterruptedException {
		GregorianCalendar time = new GregorianCalendar();
		int hour = time.get(Calendar.HOUR_OF_DAY);
		System.setProperty("webdriver.chrome.driver", "C:\\selenium chrome driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait=new WebDriverWait(driver, 50);
		driver.navigate().to("https://web.whatsapp.com/");
		driver.manage().window().maximize();
		WebElement nextPage;
		nextPage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"side\"]/div[1]/div/label/div/div[2]")));
		nextPage.sendKeys("Khara");
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"pane-side\"]/div[1]/div/div/div[10]/div/div/div[2]/div[1]/div[1]/span/span")).click();
		WebElement textBox;
		textBox = driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]"));
		if (hour < 12) {
			textBox.sendKeys("Good Morning");
		}
		else if (hour >= 12 && hour < 17) {
			textBox.sendKeys("Good Afternoon");
		}
		else {
			textBox.sendKeys("Good Evening");
		}
		driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();
		textBox.sendKeys("Reply 1 to schedule meeting or reply 2 to cancel meeting");
		driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();
		WebElement reply;
		reply = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("_1Gy50")));
		while(true) {
			String i = reply.getText();
			System.out.println(i);
			if (i.compareTo("1") == 0) {
				textBox.sendKeys("meeting scheduled two hours from now at "+ (hour + 2) + "hours");
				break;
			}
			else if (i.compareTo("2") == 0){
				textBox.sendKeys("meeting canceled");
				break;
			}
			else continue;
		}
		driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();
		
		
		

	}

}


