package Selenium_Java;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public  class WikiTest {
	public static class Wikipedia {
		WebDriver driver;
		String link = "";
		List<String> url = new ArrayList<>();

		Wikipedia() {
			System.setProperty("webdriver.chrome.driver",
					"C:\\selenium chrome driver\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
		}

		public void openWikipedia() {
			driver.navigate().to("https://en.wikipedia.org/wiki/Main_Page");
		}

		public void pickLinks() throws InterruptedException {
			List<WebElement> links = new ArrayList<WebElement>();

			links = driver.findElements(By.xpath("//*[@id=\"mp-tfa\"]/p/a"));
			for (int i = 1; i <= links.size(); i++) {
				link = ((links.get(i - 1).getAttribute("href")));
				url.add(link);
			}
		}

		public void getLinkTitle() {
			for (int i = 0; i < url.size(); i++) {
				System.out.print("Link: " + url.get(i));
				driver.get(url.get(i));
				System.out.print("  Link Page Title: ");
				System.out.println(driver.getTitle());

			}

		}

		public static void main(String[] args) throws InterruptedException {
			Wikipedia wk = new Wikipedia();
			wk.openWikipedia();
			wk.pickLinks();
			wk.getLinkTitle();
		}

	}
	}